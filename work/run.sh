#!/bin/bash

PIDFILE=/run/openresty.pid
if [ -e $PIDFILE ]; then
	sudo kill $(cat $PIDFILE)||true
	sleep 2
fi

sudo /usr/local/sbin/openresty -c /vagrant/work/conf/nginx.conf
exit 0
