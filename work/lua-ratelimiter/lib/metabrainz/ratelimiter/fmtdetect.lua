
local ngx_req = ngx.req

local _M = {
    _VERSION = '0.01'
}


function _M.run()
	local urifmt = ""
	local args = ngx_req.get_uri_args()
	for key, val in pairs(args) do
		if key == "fmt" then
			if type(val) == "table" then
				urifmt = table.unpack(val)
			else
				urifmt = val
			end
			if urifmt == "jsonnew" then
				urifmt = "json"
			end
			break
		end
	end

	if urifmt == "" then
		local qualities = {
			["application/xml"] = 1.0,
			["application/json"] = 1.0,
			["*/*"] = 0.0,
		}

		local variants = {}
		variants["application/xml"] = "xml"
		variants["application/json"] = "json"

		-- Parse Accept header
		local accept = {}
		if not ngx_req.get_headers()["Accept"] then
			accept["*/*"] = 1
		else
			for range in ngx_req.get_headers()["Accept"]:gmatch(" *([^,]+) *") do
				local mime = range:match("([^;]+)")
				local q = range:match("q *= *([0-9.]+)") or 1.0
				accept[mime] = q
			end
		end

		if ngx_req.get_headers()["Accept"] and not ngx_req.get_headers()["Accept"]:find("q=") then
			for mime, quality in pairs(accept) do
				if mime == "*/*" then
					accept[mime] = 0.01
				elseif mime:find("*") then
					accept[mime] = 0.02
				end
			end
		end

		-- Calculate quality of each candidate
		local candidates = {}
		for mime, filename in pairs(variants) do
			q1 = accept[mime] or accept[mime:match("(%S+/)").."*"] or accept["*/*"] or 0.0
			q2 = qualities[mime] or qualities["*/*"] or 1.0
			candidates[filename] = q1 * q2
		end

		-- Find the candidate with the highest quality
		local max = 0
		local winner = ""
		for filename, quality in pairs(candidates) do
			if quality > max then
				max = quality
				winner = filename
			elseif quality == max and filename < winner then
				winner = filename
			end
		end
	else
		winner = urifmt
	end

	if not winner then
		-- force to xml or return 406 ?
		return "xml"
	end

	return winner
end


return _M
