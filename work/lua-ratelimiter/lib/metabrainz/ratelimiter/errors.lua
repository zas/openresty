local ngx = ngx

local _M = {
    _VERSION = '0.01'
}

local function senderror(msg, fmt, code)
	if fmt == "json" then
		ngx.header.content_type = "application/json"
	else
		ngx.header.content_type = "application/xml"
	end
    ngx.status = code
    ngx.say(msg)
    return ngx.exit(ngx.HTTP_OK)
end

function _M.throttled(fmt, code)
    local msg
    if fmt == "json" then
        msg = '{"error": "Your requests are being throttled by MusicBrainz because the application you are using has not identified itself. Please update your application, and see http://musicbrainz.org/doc/XML_Web_Service/Rate_Limiting for more information."}'
    else
        msg = [[
<?xml version="1.0" encoding="UTF-8"?>
<error>
  <text>Your requests are being throttled by MusicBrainz because the application you are using has not identified itself. Please update your application, and see http://musicbrainz.org/doc/XML_Web_Service/Rate_Limiting for more information.</text>
  <text>For usage, please see: http://musicbrainz.org/development/mmd</text>
</error>
]]
    end
    return senderror(msg, fmt, code)
end

function _M.ratelimitexceeded(fmt, code)
    local msg
    if fmt == "json" then
        msg = '{"error": "Your requests are exceeding the allowable rate limit. Please see http://wiki.musicbrainz.org/XMLWebService for more information."}'
    else
        msg = [[
<?xml version="1.0" encoding="UTF-8"?>
<error>
  <text>Your requests are exceeding the allowable rate limit. Please see http://wiki.musicbrainz.org/XMLWebService for more information.</text>
  <text>For usage, please see: http://musicbrainz.org/development/mmd</text>
</error>
]]
    end
    return senderror(msg, fmt, code)
end

-- function _M.serverbusy(fmt, code)
--     local msg
--     if fmt == "json" then
--         msg = '{"error": "The MusicBrainz web server is currently busy. Please try again later."}'
--     else
--         msg = [[
-- <?xml version="1.0" encoding="UTF-8"?>
-- <error>
--   <text>The MusicBrainz web server is currently busy. Please try again later.</text>
--   <text>For usage, please see: http://musicbrainz.org/development/mmd</text>
-- </error>
-- ]]
--     end
--     return senderror(msg, fmt, code)
-- end

return _M
