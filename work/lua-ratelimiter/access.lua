local fmtdetect = require "metabrainz.ratelimiter.fmtdetect"
local errors = require "metabrainz.ratelimiter.errors"

local function logerror(...)
    ngx.log(ngx.ERR, "ratelimiter error: ", ...)
end

local ua = ngx.var.http_user_agent
if not ua then
	logerror('no user-agent found')
	return errors.throttled(fmtdetect.run(), 403)
end
if string.len(ua) <= 5 then
	logerror('user-agent too short')
	return errors.throttled(fmtdetect.run(), 403)
end

local m, err = ngx.re.match(ua, '^[\\s-]*$', 'o')
if err then
	logerror(err)
	return ngx.exit(500)
end
if m then
	logerror('invalid user-agent found')
	return errors.throttled(fmtdetect.run(), 403)
end

local limit_req = require "resty.limit.req"
local limit_traffic = require "resty.limit.traffic"
local limit_conn = require "resty.limit.conn"

-- default limit of 1 request per second
local lim_default, err = limit_req.new("my_req_store", 1, 1)
assert(lim_default, err)

-- known user agents rate limit
local lim_known, err = limit_req.new("my_req_store", 45, 5)
assert(lim_known, err)

-- well known user agents rate limit
local lim_wellknown, err = limit_req.new("my_req_store", 60, 10)
assert(lim_known, err)

-- global rate limit
local lim_global, err = limit_conn.new("my_conn_store", 100, 10, 0.5)
assert(lim_global, err)

local key = ngx.var.binary_remote_addr
local lim = lim_default

local m, err = ngx.re.match(ua, "^MusicBrainz-Picard/", 'o')
if err then
	logerror(err)
	return ngx.exit(500)
end
if m then
    key = "wellknown"
    lim = lim_wellknown
else
    local m, err = ngx.re.match(ua, "headphones", 'oi')
    if err then
	    logerror(err)
	    return ngx.exit(500)
    end
    if m then
        key = "known"
        lim = lim_known
    end
end

local keys =     { key, "g" }
local limiters = { lim, lim_global }

local states = {}

local delay, err = limit_traffic.combine(limiters, keys, states)
if not delay then
    if err == "rejected" then
		return errors.ratelimitexceeded(fmtdetect.run(), 503)
	end
	logerror("failed to limit traffic: ", err)
	return ngx.exit(500)
end

if lim_global:is_committed() then
	local ctx = ngx.ctx
	ctx.limit_conn = lim_global
	ctx.limit_conn_key = keys[2]
end

if delay > 0 then
	-- print("sleeping ", delay, " sec, states: ", table.concat(states, ", "))
	ngx.sleep(delay)
end


-- vim: set tabstop=4 shiftwidth=4 expandtab:
