```
git clone --recursive git@gitlab.com:zas/openresty.git && cd openresty
vagrant up
```

From your host:
connect to http://192.168.33.10:8080

Rate limiter configuration is in `work/conf/nginx.conf`

Logs go in `work/logs`
