#!/bin/bash

sudo apt-get update

# Build dependencies for OpenResty.
sudo apt-get install -y build-essential libssl-dev libgeoip-dev
# libpcre3-dev libxml2-dev libxslt1-dev libgd-dev perl

# Install standard Nginx first so that you get the relevant service scripts installed too
#sudo apt-get install -y nginx git

USERDIR=~
BUILDDIR=$USERDIR/openresty.build
[ ! -e $BUILDDIR ] && mkdir -p $BUILDDIR

echo "Building in $BUILDDIR"
cd $BUILDDIR || exit 1

PCRE=pcre-8.37
if [ ! -d $PCRE ]; then
	TARBALL=${PCRE}.tar.bz2
	URL=ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre
	if [ ! -e $TARBALL ]; then
		wget -q $URL/$TARBALL || exit 2
	fi
	tar xjf $TARBALL || exit 3
fi

OPENRESTY=openresty-1.9.7.4
if [ ! -d $OPENRESTY ]; then
	TARBALL=${OPENRESTY}.tar.gz
	URL=https://openresty.org/download
	if [ ! -e $TARBALL ]; then
		wget -q $URL/$TARBALL || exit 2
	fi
	tar xzf $TARBALL || exit 3
fi

cd $BUILDDIR/$OPENRESTY || exit 1

export BIN=openresty
./configure \
--with-cc-opt='-g -O2 -fPIE -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2' \
--conf-path=/etc/nginx/nginx.conf \
--error-log-path=/var/log/nginx/error.log \
--http-client-body-temp-path=/var/lib/nginx/body \
--http-fastcgi-temp-path=/var/lib/nginx/fastcgi \
--http-log-path=/var/log/nginx/access.log \
--http-proxy-temp-path=/var/lib/nginx/proxy \
--http-scgi-temp-path=/var/lib/nginx/scgi \
--http-uwsgi-temp-path=/var/lib/nginx/uwsgi \
--lock-path=/var/lock/$BIN.lock \
--pid-path=/run/$BIN.pid \
--prefix=/usr/local/$BIN \
--sbin-path=/usr/local/sbin/$BIN \
--with-debug \
--with-http_addition_module \
--with-http_auth_request_module \
--with-http_geoip_module \
--with-http_gunzip_module \
--with-http_gzip_static_module \
--with-http_realip_module \
--with-http_ssl_module \
--with-http_stub_status_module \
--with-http_v2_module \
--with-ipv6 \
--with-md5=/usr/include/openssl \
--with-pcre-jit \
--with-pcre=../pcre-8.37 \
--with-sha1=/usr/include/openssl \
--with-stream \
--with-stream_ssl_module \
--with-threads

# following modules are unused
#--with-http_flv_module \
#--with-http_dav_module \
#--with-http_image_filter_module \
#--with-mail \
#--with-mail_ssl_module \
#--with-http_secure_link_module \
#--with-http_sub_module \
#--with-http_xslt_module \

make -j2
sudo make install

[ ! -d /var/lib/nginx/ ] && sudo mkdir -p /var/lib/nginx/
